@Library(value="jenkins-shared-libraries@v2", changelog=false) _
@Library(value="lpq-jenkins-shared-lib@v2.3.0", changelog=false) LPQLib

import java.util.TimeZone
import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit

import com.meridianlink.lpq.constants.ArtifactoryConstants
import com.meridianlink.lpq.constants.AzureActiveDirectoryConstants
import com.meridianlink.lpq.constants.LPQGitLabRepoConstants
import com.meridianlink.lpq.deploy.EnvironmentDeployInput

// =============================================
// Methods
// =============================================
def String getValidCloudReleaseOptionsShellCommand(String artifactoryUrl, String artifactoryReleaseRelativePath, String accessTokenEnvName) {
    return """#!/usr/bin/env bash
      # JFrog download does not include first directory
      relative_path_without_first_directory=\$(echo $artifactoryReleaseRelativePath | cut -d'/' -f2-)

      # Remove incase agent caches old builds or something
      rm -rf \$relative_path_without_first_directory 

      # Will download into folder artifactory's folder structure not including first directory (e.g: lpq-generic-local).
      # Silent download output to not pollute output.
      jfrog rt dl "$artifactoryReleaseRelativePath/*/latest/*VERSION" --access-token=\$$accessTokenEnvName --url="$artifactoryUrl" --include-dirs &> /dev/null

      cd \$relative_path_without_first_directory

      supported_cloud_releases=\$(ls */latest/VERSION)

      supported_release_names_to_display=()

      for supported_release in \${supported_cloud_releases[@]}
      do
          supported_release_names_to_display+=("\$(dirname \$(dirname \$supported_release) )")
      done

      echo \${supported_release_names_to_display[@]}
    """
}

// Removes empty, trims, lowercases, and rejoins string with newline.
def List<String> cleanUserSelectedEnvs(String userInput) {
  List<String> cleanedItems = []
  userInput.split("\\n").each {
    String trimmedInput = it.trim()

    if (trimmedInput != "") {
      cleanedItems.add(trimmedInput.toLowerCase())
    }
  };

  return cleanedItems.join("\n");
}

def String constructExpectedEnvironmentDeployFormat(Map<String, List<String>> realmRepoInfo, List<String> dedClientInfo) {
  List<String> catchAllEnvList = []
  List<String> demoDeploySetNames = ["demo", "demo-jha"]
  List<String> demoDeployList = []

  // 7pm pacific deploy set
  List<String> productionAfterHoursDeploy7pmSetNames = ["prod-st", "prod-secure", "prod-es", "prod-jha"]
  List<String> productionAfterHoursDeploy7pmList = []

  // 8pm pacific deploy set
  List<String> productionAfterHoursDeploy8pmSetNames = ["prod", "prod-cs"]
  List<String> productionAfterHoursDeploy8pmList = []

  // 9pm pacific deploy set
  List<String> productionAfterHoursDeploy9pmSetNames = ["prod-ws", "prod-hs"]
  List<String> productionAfterHoursDeploy9pmList = []

  EnvironmentDeployInput.constructExpectedSharedEnvironmentDeployFormat(realmRepoInfo).each { fullDeployEnvName ->
    String environment = fullDeployEnvName.split("/").last()

    if (productionAfterHoursDeploy7pmSetNames.contains(environment)) {
      productionAfterHoursDeploy7pmList.add(fullDeployEnvName)
    } else if (productionAfterHoursDeploy8pmSetNames.contains(environment)){
      productionAfterHoursDeploy8pmList.add(fullDeployEnvName)
    } else if (productionAfterHoursDeploy9pmSetNames.contains(environment)){
      productionAfterHoursDeploy9pmList.add(fullDeployEnvName)
    } else if (demoDeploySetNames.contains(environment)) {
      demoDeployList.add(fullDeployEnvName)
    } else {
      catchAllEnvList.add(fullDeployEnvName)
    }
  }

  catchAllEnvList.add("")

  EnvironmentDeployInput.constructExpectedDedicatedEnvironmentDeployFormat(dedClientInfo).each { fullDedEnvName ->
    catchAllEnvList.add(fullDedEnvName)
  }

  return demoDeployList + [""] + 
    productionAfterHoursDeploy7pmList + [""] + 
    productionAfterHoursDeploy8pmList + [""] + 
    productionAfterHoursDeploy9pmList + [""] + 
    catchAllEnvList
}

// Adapted from EA's shared jenkins library function "chooseEnvironment.groovy"
def void deployToWhichEnvironment(String approvalADGroup, Map<String, List<String>> realmRepoInfo, List<String> dedClientInfo, int timeoutMin) {
  List<String> envList = constructExpectedEnvironmentDeployFormat(realmRepoInfo, dedClientInfo)

  String envString = envList.join("\n")

  // Longer timeout because we need to wait for master to finish building before tag is even created.
  timeout(time: timeoutMin, unit: 'MINUTES' ) {
    String userInput = input message: 'Which environments to deploy with single version', 
      parameters: [text(defaultValue:envString, name: 'SELECTED_ENVS_TO_DEPLOY')],
      submitter: approvalADGroup

    env.SELECTED_ENVS_TO_DEPLOY = cleanUserSelectedEnvs(userInput)
  } // end timeout
}

// Jenkin's local datetime is UTC. Also scripts is blocked to not allow usage of 
// zone id in jenkins.
def Date getSystemDateTimeNow() {
  return new Date()
}

def SimpleDateFormat getLosAngelesDateTimeFormat() {
  SimpleDateFormat losAngelesDateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm a")
  TimeZone losAngelesTimeZone = TimeZone.getTimeZone("America/Los_Angeles")
  losAngelesDateTimeFormatter.setTimeZone(losAngelesTimeZone)

  return losAngelesDateTimeFormatter
}

// =============================================
// Mock run helper methods
// =============================================
def String getArtifactoryReleasePath() {
  return (env.IS_MOCK_RUN.toBoolean()) ? MockConstants.MOCK_ARTIFACTORY_RELEASE_PATH : ArtifactoryConstants.LPQ_BUILD_ARTIFACT_PATH
}

def String getArtifactoryTokenCredentialId() {
  return (env.IS_MOCK_RUN.toBoolean()) ? MockConstants.MOCK_ARTIFACTORY_TOKEN_CREDENTIAL_ID : Constants.ARTIFACTORY_TOKEN_CREDENTIAL_ID
}

def Map<String, List<String>> getRealmEnvironments() {
  return (env.IS_MOCK_RUN.toBoolean()) ? MockConstants.MOCK_REPO_REALM_ENVIRONMENTS : LPQGitLabRepoConstants.REPO_REALM_ENVIRONMENTS
}

def List<String> getDedClientAcronyms() {
  return (env.IS_MOCK_RUN.toBoolean()) ? MockConstants.MOCK_DED_CLIENT_ACRONYMS: LPQGitLabRepoConstants.DED_CLIENT_ACRONYMS
}

// =============================================
// Configurable Build Parameters
// =============================================
class MockConstants {
  static final String MOCK_ARTIFACTORY_RELEASE_PATH = "lpq-generic-local/alvin-temp-schedule-deploys"

  static final String MOCK_ARTIFACTORY_TOKEN_CREDENTIAL_ID = 'poc-artifactory-deploy-scheduler-token-id'

  static final Map MOCK_REPO_REALM_ENVIRONMENTS = [
    'lpq-infra': [
      'nonrev': ['qa', 'stage'],
      'chaos': ['perf'],
      'rev': ['demo', 'prod']
    ],
    'lpq-legacy-infra': [
      'rev': [
        'prod-cs', 'prod-es'
      ]
    ]
  ]

  static final List<String> MOCK_DED_CLIENT_ACRONYMS = [
    'pol-fire'
  ]
}

class Constants {
  static final int USER_INPUT_TIMEOUT_MIN = 5

  static final String DEPLOY_LPQ_VERSION_JOB_ABS_PATH = 'LPQ/LPQBuild/Deployment/LPQ-Deployer' // need to define branch if using multibranch pipeline mode

  static final String ARTIFACTORY_TOKEN_CREDENTIAL_ID = 'artifactory-deploy-scheduler-token-id'
}

pipeline {
  agent {
    kubernetes {
      yaml podYamlLinux(
        containerTemplates: ['jfrog-cli-v2'],
      )
    }
  }

  environment {
    // To make sure no side effects occur
    IS_DRY_RUN = true

    // To run build against mock/test resources which do not affect live deployments
    IS_MOCK_RUN = true
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '20'))
  }

  stages {
    stage('Initialize Jenkins Build') {
      steps {
        script {
          if(env.IS_DRY_RUN.toBoolean() == true) {
            String msg = "Dry run build."
            currentBuild.description = (currentBuild.description == null) ? msg : currentBuild.description + "\n${msg}"
            echo "Setting build description to ${currentBuild.description}"
          }

          if(env.IS_MOCK_RUN.toBoolean() == true) {
            String msg = "Mock run build."
            currentBuild.description = (currentBuild.description == null) ? msg : currentBuild.description + "\n${msg}"
            echo "Setting build description to ${currentBuild.description}"
          }
        }
      } //end steps
    } //end stage
    stage('Deploy to which environments?') {
      steps {
        deployToWhichEnvironment(
          AzureActiveDirectoryConstants.S_INF_P_WEB_JENKINS_LPQ_ADMIN_ID, 
          getRealmEnvironments(),
          getDedClientAcronyms(),
          Constants.USER_INPUT_TIMEOUT_MIN
        )

        script {
          deployInput.validateEnvironmentDeployInput(getRealmEnvironments(), getDedClientAcronyms(), env.SELECTED_ENVS_TO_DEPLOY)
        }

        echo "Deploying to the following environments:\n${env.SELECTED_ENVS_TO_DEPLOY}"
      }
    }
    stage('Scheduled Deploy Time') {
      when {
        expression { env.SELECTED_ENVS_TO_DEPLOY != ''}
      } //end when
      steps {
        script {
          Date localSystemDateTime = getSystemDateTimeNow()
          SimpleDateFormat losAngelesDateTimeFormatter = getLosAngelesDateTimeFormat()

          timeout(time: Constants.USER_INPUT_TIMEOUT_MIN, unit: 'MINUTES' ) {
            env.LOSANGELES_SCHEDULED_DEPLOY_DATETIME = input message: 'Scheduled Deploy Time in ', 
              parameters: [
                string(
                  name: 'LOSANGELES_SCHEDULED_DEPLOY_DATETIME', 
                  defaultValue: losAngelesDateTimeFormatter.format(localSystemDateTime),
                  description: 'Input deploy uses Los Angeles timezone.\nExample entry: 2022-11-01 07:15 pm',
                  trim: true
                )
              ],
              submitter: AzureActiveDirectoryConstants.S_INF_P_WEB_JENKINS_LPQ_ADMIN_ID
          } // end timeout

          echo "The requested Los Angeles date time ${env.LOSANGELES_SCHEDULED_DEPLOY_DATETIME}"
          Date scheduledDateTimeInSystemTimeZone = losAngelesDateTimeFormatter.parse(env.LOSANGELES_SCHEDULED_DEPLOY_DATETIME)

          long calculatedQuietPeriodSeconds = (scheduledDateTimeInSystemTimeZone.getTime() - getSystemDateTimeNow().getTime())/1000;

          if (calculatedQuietPeriodSeconds <= 0) {
            echo "Quiet Period is set to 0 due to scheduling time in the past"
            env.NEXT_JOB_QUIET_PERIOD = 0
          } else {
            echo "Adding a one minute buffer to quiet period for any time calculation inconsistencies."
            env.NEXT_JOB_QUIET_PERIOD = calculatedQuietPeriodSeconds + 60
          }

          echo "Quiet Period till next job: ${env.NEXT_JOB_QUIET_PERIOD.toInteger()/60} minutes"
        }// end script
      } //end steps
    } // end stage
    stage('Version to deploy') {
      when {
        expression { env.SELECTED_ENVS_TO_DEPLOY != null && env.SELECTED_ENVS_TO_DEPLOY != ''}
        expression { env.NEXT_JOB_QUIET_PERIOD != null && env.NEXT_JOB_QUIET_PERIOD != ''}
      } //end when
      steps {
        script {
          String accessTokenEnvName = 'accessToken'
          container('jfrog-cli-v2') {
            withCredentials([string(credentialsId: getArtifactoryTokenCredentialId(), variable: accessTokenEnvName)]) {
              // Construct options for release branch deploy
              String cloudReleaseOptions = sh(
                script: getValidCloudReleaseOptionsShellCommand(
                  constants.artifactoryURL(), 
                  getArtifactoryReleasePath(),
                  accessTokenEnvName
                ), 
                returnStdout: true
              ).trim().split(" ").join("\n")

              // Construct options for promotional deploy
              List<String> envList = EnvironmentDeployInput.constructExpectedAllEnvironmentDeployFormat(
                getRealmEnvironments(),
                getDedClientAcronyms(),
              )

              // Convert to list or minus does not work properly with arrays.
              envList = envList.minus(env.SELECTED_ENVS_TO_DEPLOY.split("\n") as List<String>)
              String promotionalEnvOptions = envList.join("\n")

              timeout(time: Constants.USER_INPUT_TIMEOUT_MIN, unit: 'MINUTES' ) {
                Map inputResponses = input message: 'Use one of the following for deploy source', 
                  parameters: [
                    choice(
                      name: 'SOURCE_PROMOTIONAL_ENV',
                      choices: "\n" + promotionalEnvOptions,
                      description: "Use this for promotional based deploy."
                    ),
                    choice(
                      name: 'MANUAL_RELEASE_TO_DEPLOY', 
                      choices: "\n" + cloudReleaseOptions,
                      description: "Use this for latest release branch based deploy."
                    )
                  ],
                  submitter: AzureActiveDirectoryConstants.S_INF_P_WEB_JENKINS_LPQ_ADMIN_ID

                if (inputResponses.SOURCE_PROMOTIONAL_ENV != null && inputResponses.SOURCE_PROMOTIONAL_ENV != '') {
                  env.SELECTED_SOURCE_PROMOTIONAL_ENV = inputResponses.SOURCE_PROMOTIONAL_ENV
                  echo "Promotional deploy - ${env.SELECTED_SOURCE_PROMOTIONAL_ENV} selected."
                } else if (inputResponses.MANUAL_RELEASE_TO_DEPLOY != null && inputResponses.MANUAL_SELECTED_RELEASE != '') {
                  env.MANUAL_SELECTED_RELEASE = inputResponses.MANUAL_RELEASE_TO_DEPLOY
                  echo "Release deploy - ${env.MANUAL_SELECTED_RELEASE} selected."
                } else {
                  error "You did not select a source promotional environment or a release branch to deploy."
                }
              } // end timeout
            }
          } // end container
        } //end script
      } // end steps
    } //end stage
    stage('Schedule Job') {
      when {
        expression { env.SELECTED_ENVS_TO_DEPLOY != null && env.SELECTED_ENVS_TO_DEPLOY != ''}
        expression { env.NEXT_JOB_QUIET_PERIOD != null && env.NEXT_JOB_QUIET_PERIOD != ''}
        expression { 
          (env.MANUAL_SELECTED_RELEASE != null && env.MANUAL_SELECTED_RELEASE != '') ||
          (env.SELECTED_SOURCE_PROMOTIONAL_ENV != null && env.SELECTED_SOURCE_PROMOTIONAL_ENV != '')
        }
      } //end when
      steps {
        script {
          def scheduledJobParameters = [
            text(
              name: "DEPLOY_ENVS",
              value: env.SELECTED_ENVS_TO_DEPLOY
            )
          ];

          String artifactoryReleasePath = getArtifactoryReleasePath()

          // Guaranteed to have promotional deploy or release branch deploy by this point.
          if (env.SELECTED_SOURCE_PROMOTIONAL_ENV != null && env.SELECTED_SOURCE_PROMOTIONAL_ENV != '') {
            scheduledJobParameters.add(
              string(
                name: "SOURCE_PROMOTION_ENV",
                value: env.SELECTED_SOURCE_PROMOTIONAL_ENV.trim()
              )
            )
          } else {
            String versionStr = sh(script: """
              relative_path_without_first_directory=\$(echo ${artifactoryReleasePath}| cut -d'/' -f2-)

              cat \$relative_path_without_first_directory/${env.MANUAL_SELECTED_RELEASE}/latest/VERSION
            """, returnStdout: true).trim()

            String vmVersionStr = sh(script: """
              relative_path_without_first_directory=\$(echo ${artifactoryReleasePath}| cut -d'/' -f2-)

              cat \$relative_path_without_first_directory/${env.MANUAL_SELECTED_RELEASE}/latest/VM-VERSION
            """, returnStdout: true).trim()

            scheduledJobParameters.add(
              string(
                name: "VERSION",
                value: versionStr
              )
            )

            scheduledJobParameters.add(
              string(
                name: "VM_VERSION",
                value: vmVersionStr
              )
            )
          }

          scheduledJobParameters.add(
            booleanParam(
              name: "IS_MOCK_RUN",
              value: env.IS_MOCK_RUN.toBoolean()
            )
          )

          if (env.IS_DRY_RUN.toBoolean() == true) {
            echo "DRY RUN ENABLED"
            echo "Schedule job ${Constants.DEPLOY_LPQ_VERSION_JOB_ABS_PATH} with parameters ${scheduledJobParameters} and quiet period of ${env.NEXT_JOB_QUIET_PERIOD}"
          } else {
            build job: Constants.DEPLOY_LPQ_VERSION_JOB_ABS_PATH, 
              parameters: scheduledJobParameters,
              quietPeriod: env.NEXT_JOB_QUIET_PERIOD,
              wait: false // just fire and forget
          }
        } //end script
      } //end steps
    } // end stage
  }
}
